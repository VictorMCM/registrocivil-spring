package com.gobmx.portalregistrocivil.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gobmx.registrocivil.modelo.entidad.Acta;
import com.gobmx.registrocivil.servicio.ActaServicio;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/")
public class ActasController
{
    public ObjectMapper map = new ObjectMapper();
    public static final Logger LOG = LoggerFactory.getLogger(ActasController.class);

    @Autowired
    @Qualifier("actaServicio")
    private ActaServicio servicio;

    @RequestMapping(path = "acta", method = RequestMethod.POST)
    public @ResponseBody Acta registrar(@RequestBody String actaAsJSON)
    {
        Acta actaEntity = new Acta();
        try
        {
            actaEntity = map.readValue(actaAsJSON, Acta.class);
            LOG.info("Se recibio: " + actaEntity.toString());
            if (servicio.agregarActa(actaEntity))
            {
                LOG.info("Se ha agregado el acta");
            } else
            {
                LOG.info("No fue posible agregar el acta");
            }
        }
        catch (Exception ex)
        {
            LOG.error("Ocurrió un error: " + ex.getMessage());
        }

        return actaEntity;
    }

    @RequestMapping(path = "acta", method = RequestMethod.PUT)
    public @ResponseBody Acta actualizar(@RequestBody String actaAsJSON)
    {
        Acta actaEntity = new Acta();
        try
        {
            actaEntity = map.readValue(actaAsJSON, Acta.class);
            LOG.info("Se recibio: " + actaEntity.toString());
            if(servicio.actualizarActa(actaEntity))
            {
                LOG.info("Se ha actualizado el acta");
            }
            else
            {
                LOG.info("No fue posible actualizar el acta");
            }
        }
        catch (Exception ex)
        {
            LOG.error("Ocurrió un error: " + ex.getMessage());
        }

        return actaEntity;
    }

    @RequestMapping(path = "acta", method = RequestMethod.DELETE)
    public @ResponseBody Acta borrar(@RequestBody String actaAsJSON)
    {
        Acta actaEntity = new Acta();
        try
        {
            actaEntity = map.readValue(actaAsJSON, Acta.class);
            LOG.info("Se recibio: " + actaEntity.toString());
            if(servicio.borrarActa(actaEntity))
            {
                LOG.info("Se ha borrado el acta");
            }
            else
            {
                LOG.info("No fue posible borrar el acta");
            }
        }
        catch (Exception ex)
        {
            LOG.error("Ocurrió un error: " + ex.getMessage());
        }

        return actaEntity;
    }

    @RequestMapping(path = "acta/{id}", method = RequestMethod.GET)
    public @ResponseBody Acta obtenerActa(@PathVariable(value = "id") Long id)
    {
        try
        {
            LOG.info("Se recibio el id: " + id);
            Acta actaEntity = servicio.obtenerActa(id);
            if(actaEntity != null)
            {
                return actaEntity;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            LOG.error("Ocurrió un error: " + ex.getMessage());
            return null;
        }
    }

    @GetMapping("acta")
    public @ResponseBody List<Acta> listarActas()
    {
        return servicio.listarActas();
    }
}
