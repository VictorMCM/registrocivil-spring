package com.gobmx.portalregistrocivil;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.gobmx"})
public class PortalRegistroCivilApplication
{

    public static void main(String[] args)
    {
        SpringApplication.run(PortalRegistroCivilApplication.class, args);
    }
}
