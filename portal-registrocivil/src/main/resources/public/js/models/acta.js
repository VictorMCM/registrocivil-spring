var regCivil = angular.module('actaApp',[]);

regCivil.controller('actasController', function($scope, $http){
    $scope.acta = {};
    $scope.actas = [];
    $scope.accion = 0;

    $scope.estados = [
        {estado :"Aguascalientes", codigo : "AS"},
        {estado :"Baja California", codigo : "BC"},
        {estado :"Baja California Sur", codigo : "BS"},
        {estado :"Campeche", codigo : "CC"},
        {estado :"Chiapas", codigo : "CS"},
        {estado :"Chihuahua", codigo : "CH"},
        {estado :"Ciudad de México", codigo : "DF"},
        {estado :"Coahuila", codigo : "CL"},
        {estado :"Colima", codigo : "CM"},
        {estado :"Durango", codigo : "DG"},
        {estado :"Estado de México", codigo : "MC"},
        {estado :"Guanajuato", codigo : "GT"},
        {estado :"Guerrero", codigo : "GR"},
        {estado :"Hidalgo", codigo : "HG"},
        {estado :"Jalisco", codigo : "JC"},
        {estado :"Michoacán", codigo : "MN"},
        {estado :"Morelos", codigo : "MS"},
        {estado :"Nayarit", codigo : "NT"},
        {estado :"Nuevo León", codigo : "NL"},
        {estado :"Oaxaca", codigo : "OC"},
        {estado :"Puebla", codigo : "PL"},
        {estado :"Querétaro", codigo : "QT"},
        {estado :"Quintana Roo", codigo : "QR"},
        {estado :"San Luis Potosí", codigo : "SP"},
        {estado :"Sinaloa", codigo : "SL"},
        {estado :"Sonora", codigo : "SR"},
        {estado :"Tabasco", codigo : "TC"},
        {estado :"Tamaulipas", codigo : "TS"},
        {estado :"Tlaxcala", codigo : "TL"},
        {estado :"Veracruz", codigo : "VZ"},
        {estado :"Yucatán", codigo : "YN"},
        {estado :"Zacatecas", codigo : "ZS"}
        ];

    $scope.Seccion = function (accion) {
        return accion === $scope.accion;
    };

    $scope.verSeccion = function (accion){
        $scope.accion = accion;
        if(accion === 1){
            $scope.acta = {};
        }
    };

    $scope.prepararActualizacion = function (datos){
        datos.fregistro = new Date(datos.fregistro);
        datos.fnacimientoR = new Date(datos.fnacimientoR);
        $scope.acta = datos;
        $scope.verSeccion(3);
    };

    $scope.resetForm = function(){
        $scope.acta = {};
        $scope.FormRegistro.$setPristine();
    };

    $scope.listarActas = function(){
        $http({
            url:'/acta', method: 'GET'
        }).then(function(response){
            $scope.actas = response.data;
        }, function(response){
            console.log(response);
        });
    };

    $scope.verDetalles = function(idActa){
        $http({
            url:'/acta/'+idActa , method: 'GET'
        }).then(function(response){
            $scope.acta = response.data;
            $scope.acta.fregistro = new Date(response.data.fregistro);
            $scope.acta.fnacimientoR = new Date(response.data.fnacimientoR);
            $scope.verSeccion(2);
        }, function(response){
            console.log(response);
            $scope.listarActas();
            $scope.verSeccion(0);
        });
    };

    $scope.borrarActa = function(datos){
        $http({
            url:'/acta', method: 'DELETE', data: datos
        }).then(function(response){
            $scope.listarActas();
            $scope.verSeccion(0);
        }, function(response){
            console.log(response);
            $scope.listarActas();
            $scope.verSeccion(0);
        });
    };

    $scope.guardarActa = function (datos) {
        $http({
           url:'/acta', method: 'POST', data: datos
        }).then(function (response) {
            $scope.resetForm();
            $scope.acta = {};
            $scope.listarActas();
            $scope.verSeccion(0);
        }, function (response) {
            $scope.acta = {};
            $scope.resetForm();
        });
    };

    $scope.actualizarActa = function (datos){
        $http({
            url:'/acta', method:'PUT', data: datos
        }).then(function (response){
            $scope.acta = {};
            $scope.listarActas();
            $scope.verSeccion(0);
        }, function (response){
            $scope.acta = {};
            $scope.listarActas();
            $scope.verSeccion(0);
        });
    };
});