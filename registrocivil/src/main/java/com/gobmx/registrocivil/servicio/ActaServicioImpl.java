package com.gobmx.registrocivil.servicio;

import com.gobmx.registrocivil.data.ActaRepository;
import com.gobmx.registrocivil.modelo.entidad.Acta;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Service("actaServicio")
public class ActaServicioImpl implements ActaServicio
{
    @Autowired
    private ActaRepository repo;

    private static final Logger log = LoggerFactory.getLogger(ActaServicioImpl.class);

    private Random azar = new Random();

    int[] valorSumaLetras = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 2, 3, 4, 5, 6, 7, 8};

    @Override
    public boolean agregarActa(Acta acta)
    {
        if (acta != null)
        {
            Acta actaCompleta = completarActaNueva(acta);
            if (actaCompleta != null)
            {
                repo.save(actaCompleta);
                return true;
            }
            return false;
        }
        return false;
    }

    @Override
    public boolean actualizarActa(Acta acta)
    {
        if (acta != null)
        {
            Acta actaCompleta = actualizarYCompletarActa(acta);
            if (actaCompleta != null)
            {
                repo.save(actaCompleta);
                return true;
            }
            return false;
        }
        return false;
    }

    @Override
    public boolean borrarActa(Acta acta)
    {
        if (acta != null)
        {
            repo.delete(acta);
            return true;
        }

        return false;
    }

    @Override
    public Acta obtenerActa(Long identificador)
    {
        return repo.findById(identificador).get();
    }

    @Override
    public List<Acta> listarActas()
    {
        return (List<Acta>) repo.findAll();
    }

    private String determinarNumero(String letra)
    {
        try
        {
            int numero = 0;

            switch (letra.toUpperCase())
            {
                case "A":
                    numero = valorSumaLetras[0];
                    break;
                case "B":
                    numero = valorSumaLetras[1];
                    break;
                case "C":
                    numero = valorSumaLetras[2];
                    break;
                case "D":
                    numero = valorSumaLetras[3];
                    break;
                case "E":
                    numero = valorSumaLetras[4];
                    break;
                case "F":
                    numero = valorSumaLetras[5];
                    break;
                case "G":
                    numero = valorSumaLetras[6];
                    break;
                case "H":
                    numero = valorSumaLetras[7];
                    break;
                case "I":
                    numero = valorSumaLetras[8];
                    break;
                case "J":
                    numero = valorSumaLetras[9];
                    break;
                case "K":
                    numero = valorSumaLetras[10];
                    break;
                case "L":
                    numero = valorSumaLetras[11];
                    break;
                case "M":
                    numero = valorSumaLetras[12];
                    break;
                case "N":
                    numero = valorSumaLetras[13];
                    break;
                case "O":
                    numero = valorSumaLetras[14];
                    break;
                case "P":
                    numero = valorSumaLetras[15];
                    break;
                case "Q":
                    numero = valorSumaLetras[16];
                    break;
                case "R":
                    numero = valorSumaLetras[17];
                    break;
                case "S":
                    numero = valorSumaLetras[18];
                    break;
                case "T":
                    numero = valorSumaLetras[19];
                    break;
                case "U":
                    numero = valorSumaLetras[20];
                    break;
                case "V":
                    numero = valorSumaLetras[21];
                    break;
                case "W":
                    numero = valorSumaLetras[22];
                    break;
                case "X":
                    numero = valorSumaLetras[23];
                    break;
                case "Y":
                    numero = valorSumaLetras[24];
                    break;
                case "Z":
                    numero = valorSumaLetras[25];
                    break;
                default:
                    numero = 0;
                    break;
            }

            return String.valueOf(numero);
        }
        catch (Exception ex)
        {
            log.warn(ex.getMessage());

            return "0";
        }
    }

    private Acta completarActaNueva(Acta actaI)
    {
        try
        {
            SimpleDateFormat formato = new SimpleDateFormat("yyMMdd");
            SimpleDateFormat formatoRegistro = new SimpleDateFormat("yyyy/MM/dd");
            SimpleDateFormat formatoIdentificador = new SimpleDateFormat("yyyyMMddHHmmss");
            Date fRegistroDate = new Date();
            String idString = formatoIdentificador.format(fRegistroDate);
            Long identificador = Long.valueOf(idString);
            String CURP = "";
            CURP += !actaI.getApellidoPR().substring(0, 2).toUpperCase().equals("  ") ? actaI.getApellidoPR().substring(0, 2).toUpperCase() : "XX";
            CURP += !actaI.getApellidoMR().substring(0, 1).toUpperCase().equals(" ") ? actaI.getApellidoMR().substring(0, 1).toUpperCase() : "X";
            CURP += !actaI.getNombreR().substring(0, 1).toUpperCase().equals(" ") ? actaI.getNombreR().substring(0,1).toUpperCase() : "X";
            CURP += formato.format(actaI.getFNacimientoR());
            CURP += actaI.getSexoR().toUpperCase();
            CURP += actaI.getEstado().toUpperCase();
            CURP += !actaI.getApellidoPR().substring(2, 3).toUpperCase().equals(" ") ? actaI.getApellidoPR().substring(2, 3).toUpperCase() : "X";
            CURP += !actaI.getApellidoMR().substring(2, 3).toUpperCase().equals(" ") ? actaI.getApellidoMR().substring(2, 3).toUpperCase() : "X";
            CURP += !actaI.getNombreR().substring(2, 3).toUpperCase().equals(" ") ? actaI.getNombreR().substring(2, 3).toUpperCase() : "X";
            CURP += String.valueOf(azar.nextInt(10));
            CURP += String.valueOf(azar.nextInt(10));

            log.info("Esta es la CURP: " + CURP);

            int oficialia = Integer.valueOf(formatoRegistro.format(fRegistroDate).substring(8));
            String fRegistro = formatoRegistro.format(fRegistroDate);
            String noActa1 = determinarNumero(CURP.substring(0, 1));
            String noActa2 = determinarNumero(CURP.substring(1, 2));
            String noActa3 = determinarNumero(CURP.substring(2, 3));
            String noActa4 = determinarNumero(CURP.substring(3, 4));

            int noActa = Integer.valueOf((noActa1 + noActa2 + noActa3 + noActa4));

            return new Acta(identificador, CURP, actaI.getEstado(), oficialia, fRegistro, actaI.getLibro(), noActa, actaI.getAnotaciones(), actaI.getNombreR(), actaI.getApellidoPR(), actaI.getApellidoMR(), actaI.getSexoR(), actaI.getFNacimientoR(), actaI.getLNacimientoR(), actaI.getNombreP(), actaI.getApellidoPP(), actaI.getApellidoMP(), actaI.getNacionalidadP(), actaI.getNombreM(), actaI.getApellidoPM(), actaI.getApellidoMM(), actaI.getNacionalidadM());
        }
        catch (Exception ex)
        {
            log.error("Error al completar Acta: " + ex.getMessage());

            return null;
        }
    }

    private Acta actualizarYCompletarActa(Acta actaI)
    {
        try
        {
            SimpleDateFormat formato = new SimpleDateFormat("yyMMdd");

            String CURP = "";
            CURP += !actaI.getApellidoPR().substring(0, 2).toUpperCase().equals("  ") ? actaI.getApellidoPR().substring(0, 2).toUpperCase() : "XX";
            CURP += !actaI.getApellidoMR().substring(0, 1).toUpperCase().equals(" ") ? actaI.getApellidoMR().substring(0, 1).toUpperCase() : "X";
            CURP += !actaI.getNombreR().substring(0, 1).toUpperCase().equals(" ") ? actaI.getNombreR().substring(0,1).toUpperCase() : "X";
            CURP += formato.format(actaI.getFNacimientoR());
            CURP += actaI.getSexoR().toUpperCase();
            CURP += actaI.getEstado().toUpperCase();
            CURP += !actaI.getApellidoPR().substring(2, 3).toUpperCase().equals(" ") ? actaI.getApellidoPR().substring(2, 3).toUpperCase() : "X";
            CURP += !actaI.getApellidoMR().substring(2, 3).toUpperCase().equals(" ") ? actaI.getApellidoMR().substring(2, 3).toUpperCase() : "X";
            CURP += !actaI.getNombreR().substring(2, 3).toUpperCase().equals(" ") ? actaI.getNombreR().substring(2, 3).toUpperCase() : "X";
            CURP += String.valueOf(azar.nextInt(10));
            CURP += String.valueOf(azar.nextInt(10));

            log.info("La CURP cambio de: " + actaI.getCURP() + " a: " + CURP);

            return new Acta(actaI.getIdentificador(), CURP, actaI.getEstado(), actaI.getOficialia(), actaI.getFRegistro(), actaI.getLibro(), actaI.getNoActa(), actaI.getAnotaciones(), actaI.getNombreR(), actaI.getApellidoPR(), actaI.getApellidoMR(), actaI.getSexoR(), actaI.getFNacimientoR(), actaI.getLNacimientoR(), actaI.getNombreP(), actaI.getApellidoPP(), actaI.getApellidoMP(), actaI.getNacionalidadP(), actaI.getNombreM(), actaI.getApellidoPM(), actaI.getApellidoMM(), actaI.getNacionalidadM());
        }
        catch (Exception ex)
        {
            log.error("Error al completar Acta: " + ex.getMessage());

            return null;
        }
    }

}
