package com.gobmx.registrocivil.servicio;

import com.gobmx.registrocivil.modelo.entidad.Acta;

import java.util.List;

public interface ActaServicio
{
    public boolean agregarActa(Acta acta);
    public boolean actualizarActa(Acta acta);
    public boolean borrarActa(Acta acta);
    public Acta obtenerActa(Long identificador);
    public List<Acta> listarActas();
}
