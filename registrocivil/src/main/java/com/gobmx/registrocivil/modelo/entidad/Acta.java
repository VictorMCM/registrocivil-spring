package com.gobmx.registrocivil.modelo.entidad;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "Actas")
public class Acta
{
    @Id
    @Column(name = "Identificador")
    private Long Identificador;
    @Column(name = "CURP")
    private String CURP;
    @Column(name = "Estado")
    private String Estado;
    @Column(name = "Oficialia")
    private int Oficialia;
    @Column(name = "FRegistro")
    private String FRegistro;
    @Column(name = "Libro")
    private int Libro;
    @Column(name = "NoActa")
    private int NoActa;
    @Column(name = "Anotaciones")
    private String Anotaciones;
    @Column(name = "NombreR")
    private String NombreR;
    @Column(name = "ApellidoPR")
    private String ApellidoPR;
    @Column(name = "ApellidoMR")
    private String ApellidoMR;
    @Column(name = "SexoR")
    private String SexoR;
    @Column(name = "FNacimientoR")
    private Date FNacimientoR;
    @Column(name = "LNacimientoR")
    private String LNacimientoR;
    @Column(name = "NombreP")
    private String NombreP;
    @Column(name = "ApellidoPP")
    private String ApellidoPP;
    @Column(name = "ApellidoMP")
    private String ApellidoMP;
    @Column(name = "NacionalidadP")
    private String NacionalidadP;
    @Column(name = "NombreM")
    private String NombreM;
    @Column(name = "ApellidoPM")
    private String ApellidoPM;
    @Column(name = "ApellidoMM")
    private String ApellidoMM;
    @Column(name = "NacionalidadM")
    private String NacionalidadM;

    public Long getIdentificador()
    {
        return Identificador;
    }

    public void setIdentificador(Long identificador)
    {
        Identificador = identificador;
    }

    public String getCURP()
    {
        return CURP;
    }

    public void setCURP(String CURP)
    {
        this.CURP = CURP;
    }

    public String getEstado()
    {
        return Estado;
    }

    public void setEstado(String estado)
    {
        Estado = estado;
    }

    public int getOficialia()
    {
        return Oficialia;
    }

    public void setOficialia(int oficialia)
    {
        Oficialia = oficialia;
    }

    public String getFRegistro()
    {
        return FRegistro;
    }

    public void setFRegistro(String FRegistro)
    {
        this.FRegistro = FRegistro;
    }

    public int getLibro()
    {
        return Libro;
    }

    public void setLibro(int libro)
    {
        Libro = libro;
    }

    public int getNoActa()
    {
        return NoActa;
    }

    public void setNoActa(int noActa)
    {
        NoActa = noActa;
    }

    public String getAnotaciones()
    {
        return Anotaciones;
    }

    public void setAnotaciones(String anotaciones)
    {
        Anotaciones = anotaciones;
    }

    public String getNombreR()
    {
        return NombreR;
    }

    public void setNombreR(String nombreR)
    {
        NombreR = nombreR;
    }

    public String getApellidoPR()
    {
        return ApellidoPR;
    }

    public void setApellidoPR(String apellidoPR)
    {
        ApellidoPR = apellidoPR;
    }

    public String getApellidoMR()
    {
        return ApellidoMR;
    }

    public void setApellidoMR(String apellidoMR)
    {
        ApellidoMR = apellidoMR;
    }

    public String getSexoR()
    {
        return SexoR;
    }

    public void setSexoR(String sexoR)
    {
        SexoR = sexoR;
    }

    public Date getFNacimientoR()
    {
        return FNacimientoR;
    }

    public void setFNacimientoR(Date FNacimientoR)
    {
        this.FNacimientoR = FNacimientoR;
    }

    public String getLNacimientoR()
    {
        return LNacimientoR;
    }

    public void setLNacimientoR(String LNacimientoR)
    {
        this.LNacimientoR = LNacimientoR;
    }

    public String getNombreP()
    {
        return NombreP;
    }

    public void setNombreP(String nombreP)
    {
        NombreP = nombreP;
    }

    public String getApellidoPP()
    {
        return ApellidoPP;
    }

    public void setApellidoPP(String apellidoPP)
    {
        ApellidoPP = apellidoPP;
    }

    public String getApellidoMP()
    {
        return ApellidoMP;
    }

    public void setApellidoMP(String apellidoMP)
    {
        ApellidoMP = apellidoMP;
    }

    public String getNacionalidadP()
    {
        return NacionalidadP;
    }

    public void setNacionalidadP(String nacionalidadP)
    {
        NacionalidadP = nacionalidadP;
    }

    public String getNombreM()
    {
        return NombreM;
    }

    public void setNombreM(String nombreM)
    {
        NombreM = nombreM;
    }

    public String getApellidoPM()
    {
        return ApellidoPM;
    }

    public void setApellidoPM(String apellidoPM)
    {
        ApellidoPM = apellidoPM;
    }

    public String getApellidoMM()
    {
        return ApellidoMM;
    }

    public void setApellidoMM(String apellidoMM)
    {
        ApellidoMM = apellidoMM;
    }

    public String getNacionalidadM()
    {
        return NacionalidadM;
    }

    public void setNacionalidadM(String nacionalidadM)
    {
        NacionalidadM = nacionalidadM;
    }

    public Acta()
    {
    }

    //Constructor para crear nuevas actas
    public Acta(String estado, int libro, String anotaciones, String nombreR, String apellidoPR, String apellidoMR, String sexoR, Date FNacimientoR, String LNacimientoR, String nombreP, String apellidoPP, String apellidoMP, String nacionalidadP, String nombreM, String apellidoPM, String apellidoMM, String nacionalidadM)
    {
        Estado = estado;
        Libro = libro;
        Anotaciones = anotaciones;
        NombreR = nombreR;
        ApellidoPR = apellidoPR;
        ApellidoMR = apellidoMR;
        SexoR = sexoR;
        this.FNacimientoR = FNacimientoR;
        this.LNacimientoR = LNacimientoR;
        NombreP = nombreP;
        ApellidoPP = apellidoPP;
        ApellidoMP = apellidoMP;
        NacionalidadP = nacionalidadP;
        NombreM = nombreM;
        ApellidoPM = apellidoPM;
        ApellidoMM = apellidoMM;
        NacionalidadM = nacionalidadM;
    }

    //Constructor para actualizar actas
    public Acta(Long identificador, String estado, int libro, String anotaciones, String nombreR, String apellidoPR, String apellidoMR, String sexoR, Date FNacimientoR, String LNacimientoR, String nombreP, String apellidoPP, String apellidoMP, String nacionalidadP, String nombreM, String apellidoPM, String apellidoMM, String nacionalidadM)
    {
        Identificador = identificador;
        Estado = estado;
        Libro = libro;
        Anotaciones = anotaciones;
        NombreR = nombreR;
        ApellidoPR = apellidoPR;
        ApellidoMR = apellidoMR;
        SexoR = sexoR;
        this.FNacimientoR = FNacimientoR;
        this.LNacimientoR = LNacimientoR;
        NombreP = nombreP;
        ApellidoPP = apellidoPP;
        ApellidoMP = apellidoMP;
        NacionalidadP = nacionalidadP;
        NombreM = nombreM;
        ApellidoPM = apellidoPM;
        ApellidoMM = apellidoMM;
        NacionalidadM = nacionalidadM;
    }

    //Constructor para generar actas completas
    public Acta(Long identificador, String CURP, String estado, int oficialia, String FRegistro, int libro, int noActa, String anotaciones, String nombreR, String apellidoPR, String apellidoMR, String sexoR, Date FNacimientoR, String LNacimientoR, String nombreP, String apellidoPP, String apellidoMP, String nacionalidadP, String nombreM, String apellidoPM, String apellidoMM, String nacionalidadM)
    {
        Identificador = identificador;
        this.CURP = CURP;
        Estado = estado;
        Oficialia = oficialia;
        this.FRegistro = FRegistro;
        Libro = libro;
        NoActa = noActa;
        Anotaciones = anotaciones;
        NombreR = nombreR;
        ApellidoPR = apellidoPR;
        ApellidoMR = apellidoMR;
        SexoR = sexoR;
        this.FNacimientoR = FNacimientoR;
        this.LNacimientoR = LNacimientoR;
        NombreP = nombreP;
        ApellidoPP = apellidoPP;
        ApellidoMP = apellidoMP;
        NacionalidadP = nacionalidadP;
        NombreM = nombreM;
        ApellidoPM = apellidoPM;
        ApellidoMM = apellidoMM;
        NacionalidadM = nacionalidadM;
    }

    @Override
    public String toString()
    {
        return "Acta{" +
                "Identificador=" + Identificador +
                ", CURP='" + CURP + '\'' +
                ", Estado='" + Estado + '\'' +
                ", Oficialia=" + Oficialia +
                ", FRegistro='" + FRegistro + '\'' +
                ", Libro=" + Libro +
                ", NoActa=" + NoActa +
                ", Anotaciones='" + Anotaciones + '\'' +
                ", NombreR='" + NombreR + '\'' +
                ", ApellidoPR='" + ApellidoPR + '\'' +
                ", ApellidoMR='" + ApellidoMR + '\'' +
                ", SexoR='" + SexoR + '\'' +
                ", FNacimientoR=" + FNacimientoR +
                ", LNacimientoR='" + LNacimientoR + '\'' +
                ", NombreP='" + NombreP + '\'' +
                ", ApellidoPP='" + ApellidoPP + '\'' +
                ", ApellidoMP='" + ApellidoMP + '\'' +
                ", NacionalidadP='" + NacionalidadP + '\'' +
                ", NombreM='" + NombreM + '\'' +
                ", ApellidoPM='" + ApellidoPM + '\'' +
                ", ApellidoMM='" + ApellidoMM + '\'' +
                ", NacionalidadM='" + NacionalidadM + '\'' +
                '}';
    }
}