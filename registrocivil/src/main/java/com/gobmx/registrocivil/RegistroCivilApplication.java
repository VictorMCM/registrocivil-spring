package com.gobmx.registrocivil;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.gobmx"})
public class RegistroCivilApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(RegistroCivilApplication.class, args);
    }
}
