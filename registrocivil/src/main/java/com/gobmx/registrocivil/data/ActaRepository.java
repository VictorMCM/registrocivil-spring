package com.gobmx.registrocivil.data;

import com.gobmx.registrocivil.modelo.entidad.Acta;
import org.springframework.data.repository.CrudRepository;

import java.io.Serializable;

public interface ActaRepository extends CrudRepository<Acta, Serializable>
{
}
