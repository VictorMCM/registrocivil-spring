package com.gobmx.registrocivil;

import com.gobmx.registrocivil.modelo.entidad.Acta;
import com.gobmx.registrocivil.servicio.ActaServicio;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.sql.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = RegistroCivilApplication.class, loader = AnnotationConfigContextLoader.class)
public class RegistroCivilApplicationTests
{
    private static final Logger log = LoggerFactory.getLogger(RegistroCivilApplicationTests.class);

    @Autowired
    @Qualifier("actaServicio")
    private ActaServicio servicio;

    @Test
    public void agregarActaPrueba()
    {
        try
        {
            log.info("--------Iniciando registro--------");
            //Acta acta = new Acta("MC", 16, "Sin anotaciones", "Victor Manuel", "Castrejon", "Martinez", "H", Date.valueOf("1993-08-18"), "Bo. Mineros Chimalhuacan", "Eligio", "Castrejon", "Castrejon", "Mexicana", "Laura", "Martinez", "Cedillo", "Mexicana");
            Acta acta = new Acta("GR", 16, "Sin anotaciones", "Ramon", "Hernandez", "Ramirez", "H", Date.valueOf("1995-08-31"), "Bo. Mineros Chimalhuacan", "Eligio", "Castrejon", "Castrejon", "Mexicana", "Laura", "Martinez", "Cedillo", "Mexicana");
            log.info("Acta previa:\n" + acta.toString());
            if(servicio.agregarActa(acta))
            {
                log.info("Se ha generado el acta correctamente");
            }
            else
            {
                log.warn("No se pudo generar el acta");
            }
        }
        catch (Exception ex)
        {
            log.error("Error al generar el acta: \n" + ex.getMessage());
        }
    }

    @Test
    public void obtenerActa()
    {
        try
        {
            log.info("--------Iniciando lectura--------");
            Acta acta = servicio.obtenerActa(20180719213740L);
            if(acta != null)
            {
                log.info("Se obtuvieron los siguientes datos: \n" + acta.toString());
            }
            else
            {
                log.warn("No se obtuvo ningun dato");
            }
        }
        catch (Exception ex)
        {
            log.error("Error al obtener el acta: \n" + ex.getMessage());
        }
    }

    /*@Test
    public void borrarActa()
    {
        try
        {
            log.info("--------Iniciando borrado--------");
            Acta acta = servicio.obtenerActa(20180719213740L);
            if(acta != null)
            {
                if(servicio.borrarActa(acta))
                {
                    log.info("Se borraron los siguientes datos: \n" + acta.toString());
                }

                else
                {
                    log.warn("No se borro ningun dato");
                }
            }
            else
            {
                log.warn("No se borro ningun dato");
            }
        }
        catch (Exception ex)
        {
            log.error("Error al borrar el acta: \n" + ex.getMessage());
        }
    }*/

    @Test
    public void actualizarActa()
    {
        try
        {
            log.info("--------Iniciando actualizacion--------");
            Acta acta = servicio.obtenerActa(20180720092223L);

            if(acta != null)
            {
                acta.setEstado("MN");
                acta.setLibro(16);
                acta.setAnotaciones("El registro a cambiado");
                acta.setNombreR("El nuevo nombre");
                acta.setApellidoPR("El nuevo apellido p");
                acta.setApellidoMR("El nuevo apellido m");
                acta.setSexoR("H");
                acta.setFNacimientoR(Date.valueOf("1989-10-08"));
                acta.setLNacimientoR("Uruapan, Michoacan");
                acta.setNombreP("El nuevo papa");
                acta.setApellidoPP("El apellido p del papa");
                acta.setApellidoMP("El apellido m del papa");
                acta.setNacionalidadP("Gringa");
                acta.setNombreM("La nueva mama");
                acta.setApellidoPM("El apellido p de la mama");
                acta.setApellidoMM("El apellido m de la mama");
                acta.setNacionalidadM("Francesa");

                log.info("Acta previa actualización:\n" + acta.toString());

                if(servicio.actualizarActa(acta))
                {
                    log.info("Se ha actualizado el acta correctamente");
                }
                else
                {
                    log.warn("No se pudo actualizar el acta");
                }
            }

            else
            {
                log.warn("Imposible obtener el registro");
            }
        }
        catch (Exception ex)
        {
            log.error("Error al actualizar el acta: \n" + ex.getMessage());
        }
    }

    @Test
    public void listarActas()
    {
        try
        {
            log.info("--------Iniciando listado--------");
            List<Acta> actas = servicio.listarActas();
            if(actas != null)
            {
                for (Acta acta : actas)
                {
                    log.info(acta.toString());
                }
            }
            else
            {
                log.warn("No se obtuvieron actas");
            }
        }
        catch (Exception ex)
        {
            log.error("Error al listar actas: " + ex.getMessage());
        }
    }
}